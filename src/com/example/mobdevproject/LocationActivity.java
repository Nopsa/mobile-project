package com.example.mobdevproject;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class LocationActivity extends Activity implements SensorEventListener {

	// define the display assembly compass picture
	private ImageView image;

	// record the compass picture angle turned
	private float currentDegree = 0f;

	// device sensor manager
	private SensorManager mSensorManager;

	TextView tvHeading;

	private Address destinationAddress = null;
	
	private Location curLocation = null;

	/**
	 * Finds the closest address containing the name given in destination.
	 * 
	 * @param destination
	 *            the address of the destination.
	 */
	private void findAdress(String destination) {
		TextView distanceText;
		distanceText = (TextView) findViewById(R.id.distanceToDestination);

		LocationManager lm = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);

		Criteria crit = new Criteria();
		crit.setAccuracy(Criteria.ACCURACY_FINE);
		String provider = lm.getBestProvider(crit, true);
		Location location = lm.getLastKnownLocation(provider);
		
		List<Address> addresses = null;
		try {
			addresses = (new Geocoder(this)).getFromLocationName(destination,
					Integer.MAX_VALUE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (addresses == null) {
			return;
		}
		if (location == null) {
			return;
		}
		
		this.curLocation = location;
		float closestDistance = Float.MAX_VALUE;

		for (Address adr : addresses) {
			float[] result = new float[1];

			Location.distanceBetween(location.getLatitude(),
					location.getLongitude(), adr.getLatitude(),
					adr.getLongitude(), result);

			float distance = result[0];

			if (distance < closestDistance) {
				closestDistance = distance;
				this.destinationAddress = adr;
			}
		}
		distanceText.setText(Float.toString(closestDistance));
	}

	/**
	 * Re calculates the distance from device to destination when the device is
	 * moved.
	 * 
	 * @param location
	 *            the current location of the device
	 */
	public void updateDistance(Location location) {
		TextView distanceText;
		distanceText = (TextView) findViewById(R.id.distanceToDestination);
		
		this.curLocation = location;
		float[] result = new float[1];

		Location.distanceBetween(location.getLatitude(),
				location.getLongitude(), destinationAddress.getLatitude(),
				destinationAddress.getLongitude(), result);

		float distance = result[0];

		distanceText.setText(Float.toString(distance));
	}
	
	private double getBearing()
	{
		double longDiff = Math.toRadians(destinationAddress.getLongitude() - curLocation.getLongitude());
		double la1 = Math.toRadians(curLocation.getLatitude());
		double la2 = Math.toRadians(destinationAddress.getLatitude());
		double y = Math.sin(longDiff) * Math.cos(la2);
		double x = Math.cos(la1) * Math.sin(la2) - Math.sin(la1) * Math.cos(la2) * Math.cos(longDiff);

		double result = Math.toDegrees(Math.atan2(y, x));
		return (result +360.0) % 360.0;
	}

	/**
	 * Set up the listener for location change.
	 */
	private void checkForLoactionUpdate() {
		LocationManager lm = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);

		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				updateDistance(location);
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};

		lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
				locationListener);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);

		Intent intent = getIntent();
		String destination = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

		findAdress(destination);
		checkForLoactionUpdate();

		image = (ImageView) findViewById(R.id.imageViewCompass);
		tvHeading = (TextView) findViewById(R.id.tvHeading);
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.location, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(this,
				mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
				SensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// get the angle around the z-axis rotated
		float degree = Math.round(event.values[0]);

		tvHeading.setText("Heading: " + Float.toString(degree) + " degrees" );

		// create a rotation animation (reverse turn degree degrees)
		RotateAnimation ra = new RotateAnimation(currentDegree, -degree,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);

		// how long the animation will take place
		ra.setDuration(210);

		// set the animation after the end of the reservation status
		ra.setFillAfter(true);

		// Start the animation
		image.startAnimation(ra);
		currentDegree = -degree;

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// not in use
	}
}
